from __future__ import print_function
from __future__ import division

import abc
import platform
import numpy as np
from audio_visualizer import config

_gamma = np.load(config.GAMMA_TABLE_PATH)
"""Gamma lookup table used for nonlinear brightness correction"""

_prev_pixels = np.tile(253, (3, config.N_PIXELS))
"""Pixel values that were most recently displayed on the LED strip"""

pixels = np.tile(1, (3, config.N_PIXELS))
"""Pixel values for the LED strip"""

_is_python_2 = int(platform.python_version_tuple()[0]) == 2

class AbstractDevice(abc.ABC):
    @abc.abstractmethod
    def start(self):
        pass

    @abc.abstractmethod
    def update(self):
        pass

class Esp8266(AbstractDevice):
    def __init__(self):
        pass

    def start(self):
        import socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def update(self):
        """Sends UDP packets to ESP8266 to update LED strip values

        The ESP8266 will receive and decode the packets to determine what values
        to display on the LED strip. The communication protocol supports LED strips
        with a maximum of 256 LEDs.

        The packet encoding scheme is:
            |i|r|g|b|
        where
            i (0 to 255): Index of LED to change (zero-based)
            r (0 to 255): Red value of LED
            g (0 to 255): Green value of LED
            b (0 to 255): Blue value of LED
        """
        global pixels, _prev_pixels
        # Truncate values and cast to integer
        pixels = np.clip(pixels, 0, 255).astype(int)
        # Optionally apply gamma correc tio
        p = _gamma[pixels] if config.SOFTWARE_GAMMA_CORRECTION else np.copy(pixels)
        MAX_PIXELS_PER_PACKET = 126
        # Pixel indices
        idx = range(pixels.shape[1])
        idx = [i for i in idx if not np.array_equal(p[:, i], _prev_pixels[:, i])]
        n_packets = len(idx) // MAX_PIXELS_PER_PACKET + 1
        idx = np.array_split(idx, n_packets)
        for packet_indices in idx:
            m = '' if _is_python_2 else []
            for i in packet_indices:
                if _is_python_2:
                    m += chr(i) + chr(p[0][i]) + chr(p[1][i]) + chr(p[2][i])
                else:
                    m.append(i)  # Index of pixel to change
                    m.append(p[0][i])  # Pixel red value
                    m.append(p[1][i])  # Pixel green value
                    m.append(p[2][i])  # Pixel blue value
            m = m if _is_python_2 else bytes(m)
            self._sock.sendto(m, (config.UDP_IP, config.UDP_PORT))
        _prev_pixels = np.copy(p)


esp8266_device= Esp8266()

# ESP8266 uses WiFi communication
if config.DEVICE == 'esp8266':
    esp8266_device.start()
else:
    raise ValueError('Invalid device selected')

def update():
    """Updates the LED strip values"""
    if config.DEVICE == 'esp8266':
        esp8266_device.update()
    else:
        raise ValueError('Invalid device selected')

def run_test():
    '''
    If everything is working, you should see a red, green, and blue pixel scroll across the LED strip continously
    '''
    import time
    # Turn all pixels off
    pixels *= 0
    pixels[0, 0] = 255  # Set 1st pixel red
    pixels[1, 1] = 255  # Set 2nd pixel green
    pixels[2, 2] = 255  # Set 3rd pixel blue
    print('Starting LED strand test')
    while True:
        pixels = np.roll(pixels, 1, axis=1)
        update()
        time.sleep(.1)

# Execute this file to run a LED strand test
if __name__ == '__main__':
    run_test()


