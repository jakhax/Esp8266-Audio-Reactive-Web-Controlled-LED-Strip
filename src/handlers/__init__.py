from . import led_handler,microphone_handler,visualizers,log_handler

logger=log_handler.LogHandler().get_logger(__name__)

visualize_funcs={
    "scroll":visualizers.visualize_scroll,
    "energy":visualizers.visualize_energy,
    "spectrum":visualizers.visualize_spectrum,
}
microphone_handler=microphone_handler.MicrophoneHandler()