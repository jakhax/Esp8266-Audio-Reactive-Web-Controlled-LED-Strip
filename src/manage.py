from handlers import led_handler as led
from handlers import microphone_handler as microphone
from audio_visualizer import esp_audio_visualizer as esp
from handlers import logger

if __name__=="__main__":
    try:
        led.update()
        microphone.start_stream(esp.visualize)

    except KeyboardInterrupt as e:
        logger.exception(e)

    except Exception as e:
        logger.exception(e)
