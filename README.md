# Esp8266 Audio Reactive Web Controlled LED Strip

<img src="static/images/visualizer.jpg"  alt="Intro image" width="800" height="400">

Real-time LED strip music visualization using Python and the ESP8266, the visualization can be configured and controlled from a web interface.

**N/B** this project is not `functionally` complete yet.


## What do you need?

![logic](static/images/logic.png)

- `Computer` with Python 3.6, On Windows machines, the use of [Anaconda](https://www.continuum.io/downloads) is **highly recommended**. Anaconda simplifies the installation of Python dependencies, which is sometimes difficult on Windows.
- `ESP8266` module with RX pin exposed. I used the NodeMCU board, not tested with other boards but it should work, however be careful how you suppy power to your led strip & ESP8266.
- `WS2812b` LED strip as they are addressable, checkout [adafruit Neopixels](https://www.adafruit.com/product/1138?length=1).
- 5V `power supply` for the ws2812b.

If you decide to use [Adafruit WS2812b neopixels](https://www.adafruit.com/product/1138?length=1), they have an awesome guide on how to connect it with a microcontroller, I highly recommend reading it, here is the [link](https://learn.adafruit.com/adafruit-neopixel-uberguide?view=all).

## Installation

### Installing dependencies with Anaconda
Create a [conda virtual environment](http://conda.pydata.org/docs/using/envs.html)
```
conda create --name virtual python=3.6
activate virtual
```
Install dependencies using pip and the conda package manager
```
pip install -r requirements.txt
```

### Installing dependencies without Anaconda
- First create a virtual env.
```
python -m venv virtual
# windows
cd virtual\scripts & activate.bat
# unix
source virtual/bin/activate
```
- Install requirements
```
pip install -r requirements.txt
```
- **Note**: pyaudio may cause problems with installation but a few google searches is likely to fix this.

## Arduino dependencies
ESP8266 firmare is uploaded using the Arduino IDE. See [https://arduino-esp8266.readthedocs.io/en/latest/](https://arduino-esp8266.readthedocs.io/en/latest/) to setup the Arduino IDE for ESP8266.

After installing the Arduino IDE and ESP8266 addon, use the [Arduino Library Manager](https://www.arduino.cc/en/Guide/Libraries#toc3) to install the "WebSocketServer" library. You can download [https://github.com/JoDaNl/esp8266_ws2812_i2s](https://github.com/JoDaNl/esp8266_ws2812_i2s) library and install it in the Arduino libraries folder.

## Hardware Connections
### ESP8266 & WS2812b
- `@todo` Write Documentation.

## Setup and Configuration
- `@todo` Write Documentation.

## Audio Input 
The visualization program streams audio from the default audio input device (set by the operating system). Windows users can change the audio input device by [following these instructions](http://blogs.creighton.edu/bluecast/tips-and-tricks/set-the-default-microphone-and-adjust-the-input-volume-in-windows-7/).

### Virtual Audio Source
You can use a "virtual audio device" to transfer audio playback from one application to another. This means that you can play music on your computer and connect the playback directly into the visualization program.

### Windows
On Windows, you can use "Stereo Mix" to copy the audio output stream into the audio input. Stereo Mix is only support on certain audio chipsets. If your chipset does not support Stereo Mix, you can use a third-party application such as [Voicemeeter](http://vb-audio.pagesperso-orange.fr/Voicemeeter/).

Go to recording devices under Windows Sound settings (Control Panel -> Sound). In the right-click menu, select "Show Disabled Devices".

Enable Stereo Mix and set it as the default device. Your audio playback should now be used as the audio input source for the visualization program. If your audio chipset does not support Stereo Mix then it will not appear in the list.

### Linux
Linux users can use [Jack Audio](http://jackaudio.org/) to create a virtual audio device.

### OSX
On OSX, [Loopback](https://www.rogueamoeba.com/loopback/) can be use to create a virtual audio device.

## Running the Visualization
- `@todo` Write Documentation.

## Limitations
- The communication protocol between the computer and ESP8266 currently supports a maximum of 256 LEDs.

## Resources / Acknowlegdements
- [Scott Lawson's](https://github.com/scottlawsonbc) Audio reactive LED strip, for more details check out his [version](https://github.com/scottlawsonbc/audio-reactive-led-strip).
- Adafruit Neopixels [guide](https://learn.adafruit.com/adafruit-neopixel-uberguide?view=all) with instructions on how to connect the WS2812b to a microcontroller and best practices.
